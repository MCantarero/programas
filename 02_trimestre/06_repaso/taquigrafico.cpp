#include <stdio.h>
#include<stdlib.h>
// esto de aqui abajo es una macro variadica
#define P(...) printf(__VA_ARGS__);//cuando las macros usen valores, siempre entre parentesis para que no halla resultados erroneos

int main (int argc, char *argy[]){

    int b =37;

    b %=5;//divide el valor de b entre 5 y me da el resto de la operacion y lo guarda de nuevo en b

    P("%%: %i \n",b);

    b<<=2;//los << sirven para decir que se desplace dos bits el valor de la operacion anterior de forma que b que antes valia 2 ahora vale 8

    P("<<: %i\n",b);



return EXIT_SUCCESS;
}
